
/**
 * server konfigurasyon bilgileri
 * server in islemesi icin gerekli olan variable larin hepsini buraya giriyoruz
 * 
 */
const config = module.exports = {
  server : {
    host: 'localhost',
    port: 5000
  },
  getAllListsUrl: 'http://80.110.47.132:8080/Lists',
  getUserByUsernameUrl: 'http://10.101.102.5:8080',
  getAllUsersUrl: 'http://10.101.102.5:8080/',
  ms1: {
    host: 'https://jsonplaceholder.typicode.com/todos',
    port: 8080,
  },
  ms2: {
    host: '',
    port: '',
  },
  ms3: {
    host: '',
    port: '',
  },
};
