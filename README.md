

## Gerekli olan seyler

 - You will have to make sure that your service’s functionality is made available
to your team colleagues via a shared network

yukaridaki kisimdakini pdf ten aldim, herkesin service ini yuklemesi gerekiyor biryere ki bende bu servisleri test edebiliyim.

ihtiyacim olan

| service  | host | port  | path  |
| -------- |:----:| -----:| -----:|
| ms1      | null |  null |  null |
| ms2      | null |  null |  null |
| ms3      | null |  null |  null |

** host ve portlarin hepsine vpn araciligiyla ulasabilmeliyim, ki test edebiliyim **

bu tabelanin doldurulmasi:

ms1 path i : getUserByUsername request i yapabilecegim bir path olmali

ms2 path i : getAllTodoLists request i yapabilecegim bir path olmali.

ms3 path i : notifyUserTodoListUpdate(user, todoList) Post request i yapabilecegim bir path olmali


----------------------------



> eger ellerinde calisan bir server yok ise, yada server i baslatamiyorlar ise  request - response ornekleri gondermaleri lazim


### BSP:  


```
# request :
GET: localhost:5000

# response:
{
    "status": "up",
    "fellow_services": {
        "ms1": {
            "host": "",
            "port": ""
        },
        "ms2": {
            "host": "",
            "port": ""
        },
        "ms3": {
            "host": "",
            "port": ""
        }
    }
}
```
