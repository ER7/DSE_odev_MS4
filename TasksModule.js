/**
 * assagida import ettigimiz butun hersey disaridan kullandigimiz kütüphaneler
 * hepsinin dependency sini package.json da görebiliriz
 * 
 * those are our dependend libraries
 */
const request = require('request');
const equal = require('fast-deep-equal');
const chalk = require('chalk');
var diff = require('rus-diff').diff

let tasks = {};

// calls callback when new task set
/**
 * ilk yaptigimiz requestteki objeyi kaydediyor 
 * ikinci gönderdigimiz obje ile karsilastiriyor.
 * 
 * 
 * 
 */
const TaskChecker = (callback) => {
  return {
    setNewTask: (newTasks) => {
      callback(tasks, newTasks);
      tasks = newTasks;
    },
    checkListTask: (listId, nTasks) => {
      callback(tasks[listId], nTasks);
    }
  };
}

// callback fonksiyonu.
// 2 tane obje alip birbirlerine esit olup olmadiklarini kontrol ediyor.
const taskCheck = TaskChecker((oldT, newT) => {
  // eger iki objede birbirine esit degil ise
  if (!equal(oldT, newT)) {
    handleChanges(diff(oldT, newT));
  } else {
    // eger objeler birbirine esit ise
    process.stdout.write('.');
  }
});
function iter(obj) {
  for (var key in obj) {
    if (typeof(obj[key]) == 'object') {
      iter(obj[key]);
    } else {
      console.log(chalk.cyan(" ~ > " +  key + " : " + obj[key]));
    }
  }
}



// objenin bütün var larini print yapiyoruz.
/**
 *  eger ms1 calisiyorsa username ile datalari al, eger calismiyorsa value  yu printle.
 */
function process(key,value) {
    if (serverUp && key === 'userName') {
      getUserName(key);
      return;
    }
    console.log(chalk.cyan(key + " : "+value));
}

function traverse(o,func) {
    for (var i in o) {
        func.apply(this,[i,o[i]]);
        if (o[i] !== null && typeof(o[i])=="object") {
            //going one step down in the object tree!!
            traverse(o[i],func);
        }
    }
}


const getUserName = (name)  => {
  request({
    url: global.getUserByUsername + '/' + name,
    method: 'GET'
  }, (err, res, body) => {
    if (err) {
      console.log(`
        ${chalk.inverse(new Date().toUTCString() + " ::")} ${chalk.red('ERROR')}
        method: ${chalk.green('GET::')} url: ${chalk.blue(global.getUserByUsername + "/" + name)}`);
      return;
    }
    // print user details
    let userDetails = JSON.parse(body);
    Object.keys(userDetails).forEach((key) => {
      console.log(`${chalk.dim(key)} : ${chalk.yellow(userDetails[key])}`);
    });
  });
}


// detayli bilgi icin https://github.com/mirek/node-rus-diff
const handleChanges = (changes) => {
  // for each change action set, unset remove...
  Object.keys(changes).forEach((action) => {
    if (action === '$set') // yeni birsey eklenmis
      console.log(chalk.green('SET:'));
    if (action === '$unset') // birsey degistirilmis
      console.log(chalk.yellow('UNSET:'));
    if (action === '$remove') // bisey silinmis
      console.log(chalk.red('REMOVED:'));
    traverse(changes[action],process);
    serverUp = true;
    return;

    /**
     * 
    console.log(chalk.cyan(changes[action]));
    Object.keys(action).forEach((key) => {
      console.log(typeof(changes[action][key]));

      // get username
      console.log(key);
      if (action === '$set')
        console.log(chalk.green('SET:'));
      if (action === '$unset')
        console.log(chalk.yellow('UNSET:'));
      if (action === '$remove')
        console.log(chalk.red('REMOVED:'));

      console.log(chalk.cyan(changes[action][key]));
      // if (Object.keys(changes[action]).length > 1) {
      //   Object.keys(changes[action][key]).forEach((ch) => {
      //     if (ch === 'tasks') {
      //           console.log("-");
      //           return;
      //     }
      //     console.log(` ~> ${chalk.cyan(ch)} ${chalk.magenta(changes[action][key][ch])}`);
      //   });
      // } else {
      //   console.log(changes[action][key]);
      // }
      return;

      let userName = key.substr(0, key.indexOf('.'));
      console.log(userName);
      return;
      console.log(global.getUserByUsername + '/' + userName);
      request({
        url: global.getUserByUsername + '/' + userName,
        method: 'GET'
      }, (err, res, body) => {
        if (err) {
          console.log(`
            ${chalk.inverse(new Date().toUTCString() + " ::")} ${chalk.red('ERROR')}
            method: ${chalk.green('GET::')} url: ${chalk.blue(global.getUserByUsername + "/" + userName)}`);
          return;
        }
        // print user details
        let userDetails = JSON.parse(body);
        Object.keys(userDetails).forEach((key) => {
          console.log(`${chalk.dim(key)} : ${chalk.yellow(userDetails[key])}`);
        });
        console.log(chalk.blue('###############################################'));
        // print changes he/she made
        if (action === '$set')
          console.log(chalk.green('SET:'));
        if (action === '$unset')
          console.log(chalk.yellow('UNSET:'));
        if (action === '$remove')
          console.log(chalk.red('REMOVED:'));
        Object.keys(action[key]).forEach((ch) => {
          console.log(ch);
        });
      });
    });
    */
  });
}



let dummyToggle = true;
const dummyRes = () => {
  console.log("~");
  let lists = global.getDummyData(dummyToggle);
  dummyToggle = !dummyToggle;
  taskCheck.setNewTask(lists);
}

let serverUp = true;


// checks tasks 5 secs
/**
 * 5 saniyede bir list lere bakiyor 
 * 
 */
var checkTaskLoop = setInterval(function() {

  // server a request gönder. request(object, function);
  request({
    url: global.getAllListsUrl, // bu url e
    method: "GET", // get requesti gönder 
    timeout: 10000, // en fazla 10 sn cevap bekle
    followRedirect: true, // eger yönlendirirse git 
    maxRedirects: 10 // ama maximum 10 tane yönlendirmeye kadar
  }, (error, response, body) => {

    // eger error yoksa ve status code 200 = OK. yani hersey yolundaysa
    if (!error && response.statusCode == 200) {
      // butun listelerin bulundugu bir json objesi aliyoruz.
      let lists = JSON.parse(body); // body i json a cevir
      // cTasks =  {
      this.cTasks = {}; // ctasks diye variable olustir

      let me = this;

      /**
      // her bir list item i icin
      for( int i = 0 ; i < lists.length; i++) {
        list = lists[i];
        index = i;
      } 
      */
      lists.forEach((list, index) => {
        // { username : {
        me.cTasks[list.userName] = me.cTasks[list.userName] || {}; // eger kisinin listesi zaten varsa birsey yapma, yoksa bos bir obje olustur.
        // { username : { listid : {
        getTasks(list.id, (lTasks) => {
          me.cTasks[list.userName][list.id] = lTasks || {}  // eger listenin task i  zaten varsa birsey yapma, yoksa bos bir obje olustur.
          taskCheck.checkListTask(list.id, lTasks);
          if (index === lists.length - 1) {
            taskCheck.setNewTask(me.cTasks);
          }
        });
      });

      // eger request te birseyler yanlis gitmisse....
    } else {
      // server calismiyor diye kaydediyoruz
      if (!serverUp) {
        // dummy datayla islemimizi yapiyoruz
        dummyRes();
        return;
      }
      
      console.log(`
      ${chalk.red(':: ERROR :: ')} ${chalk.inverse(new Date().toUTCString())}
      method: ${chalk.green('GET::')} url: ${chalk.blue(global.getAllListsUrl)}`);
      serverUp = false;
      dummyRes();
    }
  });
}, 5000);

const getTasks = (id, callback) => {
  this.tasks = null;
  let me = this;
  request({
    url: `http://10.101.102.13:8080/Lists/${id}/Tasks`,
    method: 'GET'
  }, (err, res, body) => {
    if (err) {
      console.log(`
      ${chalk.inverse(new Date().toUTCString() + " ::")} ${chalk.red('ERROR')}
      method: ${chalk.green('GET::')} url: ${chalk.blue(global.getAllListsUrl + "/" + id + "/Tasks")}`);
      return;
    }
    me.tasks = JSON.parse(body);
    callback(me.tasks);
  });
}

module.exports = {
  checkTaskLoop
}
