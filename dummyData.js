// Deneme datalari, normal sartlar altinda ms2 den almamiz gereken response u simüle etmek icin
const listResponse1 = [
  {
    "tasks": [
      {
        "status": false,
        "content": "Herman",
        "id": "5a53ddfdea17a02049eb8a04"
      },
      {
        "status": false,
        "content": "Snider",
        "id": "5a53ddfd9c871f4bf13cb92a"
      },
      {
        "status": false,
        "content": "Constance",
        "id": "5a53ddfdebb9f5709b01d2d8"
      }
    ],
    "userName": "Lorene",
    "name": "Stanley",
    "id": "5a53ddfd8432b80255c1bece"
  },
  {
    "tasks": [
      {
        "status": true,
        "content": "Shari",
        "id": "5a53ddfd8773c3e5a46e47c7"
      },
      {
        "status": true,
        "content": "Chang",
        "id": "5a53ddfd56e9fa84346649a9"
      },
      {
        "status": false,
        "content": "Randolph",
        "id": "5a53ddfd25e5d2758eddd6c4"
      }
    ],
    "userName": "Cherry",
    "name": "Nichols",
    "id": "5a53ddfdfcc4dcacec6d3e38"
  },
  {
    "tasks": [
      {
        "status": true,
        "content": "Teresa",
        "id": "5a53ddfda59da6ce3be82f2c"
      },
      {
        "status": true,
        "content": "Francesca",
        "id": "5a53ddfd6dab43e8c8c84c62"
      },
      {
        "status": false,
        "content": "Ilene",
        "id": "5a53ddfd9542031506ec2609"
      }
    ],
    "userName": "Blackburn",
    "name": "Harrington",
    "id": "5a53ddfd9493112da82160af"
  },
  {
    "tasks": [
      {
        "status": false,
        "content": "Holly",
        "id": "5a53ddfd8fa13e61954f98c9"
      },
      {
        "status": false,
        "content": "Hilda",
        "id": "5a53ddfdd5cb9d427a08dd3e"
      },
      {
        "status": true,
        "content": "Horton",
        "id": "5a53ddfdbb4a3e7d4e5dd95e"
      }
    ],
    "userName": "Barlow",
    "name": "Tasha",
    "id": "5a53ddfdc09731272f967b97"
  },
  {
    "tasks": [
      {
        "status": false,
        "content": "Scott",
        "id": "5a53ddfdb3d2927befe62ef0"
      },
      {
        "status": true,
        "content": "Opal",
        "id": "5a53ddfd3c2d7dcd95cfee02"
      },
      {
        "status": false,
        "content": "Deleon",
        "id": "5a53ddfd822549cf0f66e01c"
      }
    ],
    "userName": "Elaine",
    "name": "Smith",
    "id": "5a53ddfde185b4b3e938dff0"
  },
  {
    "tasks": [
      {
        "status": true,
        "content": "Coleman",
        "id": "5a53ddfd9a8aada7108d2f46"
      },
      {
        "status": false,
        "content": "Evelyn",
        "id": "5a53ddfdccdc169c73bb1753"
      },
      {
        "status": true,
        "content": "Kelsey",
        "id": "5a53ddfd62d0d0e748713b95"
      }
    ],
    "userName": "Latisha",
    "name": "Robbie",
    "id": "5a53ddfde7bc6967fae7e3db"
  },
  {
    "tasks": [
      {
        "status": false,
        "content": "Blankenship",
        "id": "5a53ddfde2b90ffb42731d73"
      },
      {
        "status": true,
        "content": "Tamika",
        "id": "5a53ddfd59d9e72dc19b4b6e"
      },
      {
        "status": false,
        "content": "Alexandra",
        "id": "5a53ddfda99adffc622891f9"
      }
    ],
    "userName": "Gaines",
    "name": "Cecelia",
    "id": "5a53ddfdf40168c1a9b60287"
  },
  {
    "tasks": [
      {
        "status": false,
        "content": "Lori",
        "id": "5a53ddfd336f7196432a2b66"
      },
      {
        "status": false,
        "content": "Dalton",
        "id": "5a53ddfde7ac37af61193aab"
      },
      {
        "status": false,
        "content": "Lesa",
        "id": "5a53ddfd319ef915791a35a9"
      }
    ],
    "userName": "Socorro",
    "name": "Lowery",
    "id": "5a53ddfd45d29fff3945b370"
  }
];
const listResponse2 = [
  {
    "tasks": [
      {
        "status": false,
        "content": "Snider",
        "id": "5a53ddfd9c871f4bf13cb92a"
      },
      {
        "status": false,
        "content": "Constance",
        "id": "5a53ddfdebb9f5709b01d2d8"
      }
    ],
    "userName": "Lorene",
    "name": "Stanley",
    "id": "5a53ddfd8432b80255c1bece"
  },
  {
    "tasks": [
      {
        "status": true,
        "content": "Shari",
        "id": "5a53ddfd8773c3e5a46e47c7"
      },
      {
        "status": true,
        "content": "Chang",
        "id": "5a53ddfd56e9fa84346649a9"
      },
      {
        "status": false,
        "content": "Randolph",
        "id": "5a53ddfd25e5d2758eddd6c4"
      }
    ],
    "userName": "Cherry",
    "name": "Nichols",
    "id": "5a53ddfdfcc4dcacec6d3e38"
  },
  {
    "tasks": [
      {
        "status": true,
        "content": "Teresa",
        "id": "5a53ddfda59da6ce3be82f2c"
      },
      {
        "status": true,
        "content": "Francesca",
        "id": "5a53ddfd6dab43e8c8c84c62"
      },
      {
        "status": false,
        "content": "Ilene",
        "id": "5a53ddfd9542031506ec2609"
      }
    ],
    "userName": "Blackburn",
    "name": "Harrington",
    "id": "5a53ddfd9493112da82160af"
  },
  {
    "tasks": [
      {
        "status": false,
        "content": "Holly",
        "id": "5a53ddfd8fa13e61954f98c9"
      },
      {
        "status": false,
        "content": "Hilda",
        "id": "5a53ddfdd5cb9d427a08dd3e"
      },
      {
        "status": true,
        "content": "Horton",
        "id": "5a53ddfdbb4a3e7d4e5dd95e"
      }
    ],
    "userName": "Barlow",
    "name": "Tasha",
    "id": "5a53ddfdc09731272f967b97"
  },
  {
    "tasks": [
      {
        "status": false,
        "content": "Scott",
        "id": "5a53ddfdb3d2927befe62ef0"
      },
      {
        "status": true,
        "content": "Opal",
        "id": "5a53ddfd3c2d7dcd95cfee02"
      },
      {
        "status": false,
        "content": "Deleon",
        "id": "5a53ddfd822549cf0f66e01c"
      }
    ],
    "userName": "Elaine",
    "name": "Smith",
    "id": "5a53ddfde185b4b3e938dff0"
  },
  {
    "tasks": [
      {
        "status": true,
        "content": "Coleman",
        "id": "5a53ddfd9a8aada7108d2f46"
      },
      {
        "status": false,
        "content": "Evelyn",
        "id": "5a53ddfdccdc169c73bb1753"
      },
      {
        "status": true,
        "content": "Kelsey",
        "id": "5a53ddfd62d0d0e748713b95"
      }
    ],
    "userName": "Latisha",
    "name": "Robbie",
    "id": "5a53ddfde7bc6967fae7e3db"
  },
  {
    "tasks": [
      {
        "status": false,
        "content": "Blankenship",
        "id": "5a53ddfde2b90ffb42731d73"
      },
      {
        "status": true,
        "content": "Tamika",
        "id": "5a53ddfd59d9e72dc19b4b6e"
      },
      {
        "status": false,
        "content": "Alexandra",
        "id": "5a53ddfda99adffc622891f9"
      }
    ],
    "userName": "Gaines",
    "name": "Cecelia",
    "id": "5a53ddfdf40168c1a9b60287"
  }
];

// bu dosyadan 2 tane variable export ediyoruz.
module.exports = {
  listResponse2,
  listResponse1
}

