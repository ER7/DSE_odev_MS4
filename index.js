// config.js dosyasini importluyoruz
const config = require('./config.js');

/**
 * 
 * const a = 5; // birdaha degismeyecegini bildigimiz variable leri const diye tanimliyoruz 
 * var  b = 5; // var = let
 * let c = 5; // kisa zaman icerisinde degistirecegimiz, sadece fonksiyon icerisinde kullanilan variable lar icin
 */

// express server icin kullandigimiz framework Java daki spring gibi birsey.
// app i olusturup port u belirliyoruz
// https://expressjs.com
const express = require('express'); 
const app = express();

// 2 tane variable 1. tasksmodule: butun business logic in oldug kisim,
// dummyData.js: demo datalarin bulundugu dosya 
const checkTaskLoop = require('./TasksModule.js');

// config dosyamizdan ms1 ms2 ms3 dosyalarinin ip adreslerini aliyoruz
// kisacasi request yaptigimiz butun ip adresleri config de bulunuyor
/**
 * global variable bir object. 
 * 
 */
global.ms1 = config.ms1;
global.ms2 = config.ms2;
global.ms3 = config.ms3;
global.getAllListsUrl = config.getAllListsUrl;
global.getAllUsersUrl = config.getAllUsers;
global.getUserByUsername= config.getUserByUsernameUrl;



// getDummyData isimli bir fonksiyon olusturuyoruz, eger toggler true ise 1 numarali demo data degilse 2 numarali demo datayi aliyoruz
// global.getDummyData = (toggler) => { return toggler ? listResponses.listResponse1 : listResponses.listResponse2; }


//const listResponses = {
//  listResponse2,
//  listResponse1
//}
const listResponses = require('./dummyData.js');
global.getDummyData = function(toggler) {
  if (toggler) {
    return listResponses.listResponse1;
  } else {
    return listResponses.listResponse2;
  }
}


// Start server
app.listen(config.server.port, () => {
  console.log(`MS4 microservice started at
    ${config.server.host}:${config.server.port}`);
});
/**
 * localhost 5000 e get requesti geldigi zaman, bana request ve response objelerini gonder 
 * 
 */
app.get('/', (req, res) => {
  res.set({
    'Content-Type': 'application/json',
    'Content-Length': 'unknown',
    'ETag': '--'
  });

  res.json({
    status: 'up',
    fellow_services: {
      ms1: config.ms1,
      ms2: config.ms2,
      ms3: config.ms3,
    },
  });
});
